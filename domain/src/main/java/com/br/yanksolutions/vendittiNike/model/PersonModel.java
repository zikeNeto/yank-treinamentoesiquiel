package com.br.yanksolutions.vendittiNike.model;

import lombok.Data;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@Data
public class PersonModel {
    private String nick;
    private String cpf;
}
