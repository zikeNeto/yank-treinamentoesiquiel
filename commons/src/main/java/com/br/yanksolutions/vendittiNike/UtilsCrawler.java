package com.br.yanksolutions.vendittiNike;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Service
public class UtilsCrawler {


    public String aplicaRegex(String texto, Pattern REGEX) {
        return Optional.ofNullable(texto)
                .map(String::toString)
                .map(REGEX::matcher)
                .filter(Matcher::find)
                .map(Matcher::group)
                .map(String::valueOf)
                .map(String::trim)
                .map(s -> s.replace("\"", ""))
                .orElse("");
    }

    public List<String> getAllMacthesRegex(String retornoFormatada, Pattern REGEX) {
        List<String> allMatches = new ArrayList<>();
        Matcher m = REGEX.matcher(retornoFormatada);
        while (m.find()) {
            allMatches.add(m.group());
        }
        return allMatches;
    }

}
