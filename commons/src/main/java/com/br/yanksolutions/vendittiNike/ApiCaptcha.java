package com.br.yanksolutions.vendittiNike;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.*;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class ApiCaptcha {



    public String resolve(String link) throws IOException {


        MediaType mediaType = MediaType.parse("“application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType,
                "empresa=YANK&"
                        + "usuario=yank_tester&"
                        + "senha=Yank@123456&"
                        + "siteUrl="+link+"&"
                        + "siteKey=6LfglaUUAAAAAFLbHMdZUaS0QVNpJ6N0Q7bIIGpI");

        Request request = new Request.Builder()
                .url("http://workflow.yanksolutions.com.br/ws/api/v1/captcha/complexversion")
                .post(body)
                .addHeader("content-type", "application/x-www-form-urlencoded")
                .build();

        String json;
        while (true) {
            Response response = clientWSYank().newCall(request).execute();
            json = response.body().string();
            try {
                JSONObject resp = new JSONObject(json);
                return resp.getString("answer");
            } catch (Exception e) {
                System.out.println("-- ERRO AO RESOLVER CAPTCHA");
                e.printStackTrace();
            }
        }
    }

    private static OkHttpClient clientWSYank(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES);
        OkHttpClient client = new OkHttpClient();
        client = builder.build();
        return client;
    }
}
