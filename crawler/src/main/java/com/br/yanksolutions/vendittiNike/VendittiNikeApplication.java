package com.br.yanksolutions.vendittiNike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VendittiNikeApplication {

	public static void main(String[] args) {
		SpringApplication.run(VendittiNikeApplication.class, args);
	}

}
