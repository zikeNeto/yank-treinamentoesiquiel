package com.br.yanksolutions.vendittiNike.controller;

import com.br.yanksolutions.vendittiNike.exceptions.GenericException;
import com.br.yanksolutions.vendittiNike.model.PersonModel;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

public class Flow extends Thread {

    private Integer id;
    private ChromeDriver driver;
    private PersonModel personModel;

    public Flow(Integer id, ChromeDriver driver, PersonModel personModel) {
        this.id = id;
        this.driver = driver;
        this.personModel = personModel;
    }

    public void run() {
        System.out.println("<--- Start Thread ---> " + id);
        driver.get("https://www.4devs.com.br/gerador_de_nicks");

        nicknamesGenerate();
        captureAllNicknames();
        navigateToCPFGenerator();
        cpfGenerator(new PersonModel());
    }

    public void nicknamesGenerate() {

        try {

            (new WebDriverWait(driver, 240))
                    .until(ExpectedConditions.presenceOfElementLocated(By.id("method")));

            Select cboxMethod = new Select(driver.findElementById("method"));
            cboxMethod.selectByValue("random");

            (new WebDriverWait(driver, 240))
                    .until(ExpectedConditions.presenceOfElementLocated(By.id("quantity")));

            driver.findElementById("quantity").clear();
            driver.findElementById("quantity").sendKeys("50");

            Thread.sleep(240);

            Select cboxLimit = new Select(driver.findElementById("limit"));
            cboxLimit.selectByValue("8");

            Thread.sleep(240);

            driver.findElementById("bt_gerar_nick").click();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public List<PersonModel> captureAllNicknames() {
        (new WebDriverWait(driver, 240))
                .until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='nicks']/ul/li/span")));

        List<WebElement> nicknamesList = driver.findElements(By.xpath("//*[@id='nicks']/ul/li/span"));
        List<PersonModel> personList = new ArrayList<>();

        for (WebElement element : nicknamesList) {
            PersonModel person = new PersonModel();
            person.setNick(element.getText());
            personList.add(person);
        }

        return personList;
    }

    public void navigateToCPFGenerator() {
        driver.findElement(By.xpath("//*[@id='top-nav']/li/a[contains(text(), 'Gerador de CPF')]")).click();
    }

    public void cpfGenerator(PersonModel personModel) {
        (new WebDriverWait(driver, 240))
                .until(ExpectedConditions.presenceOfElementLocated(By.id("bt_gerar_cpf")));

        String cpf = null;

        driver.findElementById("bt_gerar_cpf").click();

        for (int i = 0; i < 1000; i++) {
            String cpfValue = driver.findElementById("texto_cpf").getText();

            if (!(cpfValue.isEmpty() || cpfValue.toLowerCase().startsWith("ger"))) {
                cpf = cpfValue;
                break;
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("antes do if" + cpf);
        if (cpf == null) {
            throw new GenericException("Não foi possivel extrair CPF!");
        }

        personModel.setCpf(cpf);

    }

}
