package com.br.yanksolutions.vendittiNike.factory;


import com.gargoylesoftware.htmlunit.*;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.logging.Level;

@Service
public class WebClientFactory {

    private  final long serialVersionUID = 1L;
    private  WebClient browser;


    /**
     * Método responsável por setar os valores do browser
     */

    private void setBrowser() {

        browser = new WebClient(BrowserVersion.CHROME);
        browser.getOptions().setThrowExceptionOnScriptError(false);
        browser.getOptions().setThrowExceptionOnFailingStatusCode(false);
        browser.getOptions().setRedirectEnabled(false);
        browser.getOptions().setJavaScriptEnabled(false);
        browser.getOptions().setCssEnabled(false);
        browser.getOptions().setUseInsecureSSL(true);

        java.util.logging.Logger.getLogger("com.gargoylesoftware").setLevel(Level.OFF);
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(Level.OFF);

    }

    /**
     * Método responsável por retornar o browser
     *
     * @return Retornar o browser com os valores setados
     */
    @Bean
    public  WebClient getWebClient() {
        if (browser == null) {
            setBrowser();
        }
        return browser;
    }
}
