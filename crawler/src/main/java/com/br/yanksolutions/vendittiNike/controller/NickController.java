package com.br.yanksolutions.vendittiNike.controller;

import com.br.yanksolutions.vendittiNike.model.PersonModel;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

@Controller
@EnableScheduling
public class NickController {

    @Scheduled(fixedDelay = 1000000000)
    public void startController() {
        Thread flow = new Flow(1, getDriver(1), new PersonModel());

        flow.start();
    }

    public ChromeDriver getDriver(Integer id) {
        ChromeOptions options = new ChromeOptions();

        System.setProperty("webdriver.chrome.driver", System.getProperty("os.name").toLowerCase().startsWith("windows") ? "chromedriver.exe" : "chromedriver");

        return new ChromeDriver(options);
    }
}
